import "./base.css";

import * as THREE from "three";
import "./data";

import { OrbitControls } from "three/addons/controls/OrbitControls.js";
import { EntityParams, ResultData, ShooterData, TargetData } from "./data";

const scene = new THREE.Scene();
const renderer = new THREE.WebGLRenderer();
const camera = new THREE.PerspectiveCamera(
  75,
  window.innerWidth / window.innerHeight,
  0.1,
  1000
);

window.addEventListener("resize", () => {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
});
renderer.setSize(window.innerWidth, window.innerHeight);
document.getElementById("ctx")!.appendChild(renderer.domElement);

const controls = new OrbitControls(camera, renderer.domElement);
controls.enableDamping = true;
camera.position.z = 50;

function createActor(params: EntityParams) {
  const geometry = new THREE.ConeGeometry(0.2, 0.5, 5);
  const material = new THREE.MeshBasicMaterial({
    color: new THREE.Color(
      params.entityColor.r,
      params.entityColor.g,
      params.entityColor.b
    ),
  });
  const cube = new THREE.Mesh(geometry, material);
  cube.position.setX(params.position.x);
  cube.position.setY(params.position.y);
  cube.position.setZ(params.position.z);

  scene.add(cube);

  const dirVec = new THREE.Vector3(
    params.direction.x,
    params.direction.y,
    params.direction.z
  );

  const lineHelper = new THREE.ArrowHelper(
    dirVec.clone().normalize(),
    undefined,
    dirVec.length(),
    material.color
  );
  cube.add(lineHelper);

  params.colorListeners.push((c) => {
    material.color.setRGB(c.r, c.g, c.b);
    lineHelper.setColor(new THREE.Color(c.r, c.g, c.b));
  });
  params.positionListeners.push((p) => {
    cube.position.set(p.x, p.y, p.z);
  });
  params.directionListeners.push(() => {
    const dirVec = new THREE.Vector3(
      params.direction.x,
      params.direction.y,
      params.direction.z
    );
    lineHelper.setDirection(dirVec.clone().normalize());
    lineHelper.setLength(dirVec.length());
  });

  params.focusListeners.push((pos) => {
    console.log(pos);
    controls.target = new THREE.Vector3(pos.x, pos.y, pos.z);
    controls.update();
  });

  return cube;
}

function createCollisionIndicators(params: typeof ResultData) {
  const node = new THREE.Group();
  scene.add(node);

  const geometry = new THREE.SphereGeometry(0.2, 4, 4);
  const material = new THREE.MeshBasicMaterial({
    color: new THREE.Color("#ff0"),
  });
  const collisionIndicator = new THREE.Mesh(geometry, material);
  node.add(collisionIndicator);

  const shotDirectionHelper = new THREE.ArrowHelper(
    undefined,
    undefined,
    1,
    new THREE.Color("#ff0")
  );
  node.add(shotDirectionHelper);

  function update() {
    if (!params.pointOfCollision) {
      node.visible = false;
      return;
    } else {
      node.visible = true;
    }
    collisionIndicator.position.set(
      params.pointOfCollision.x,
      params.pointOfCollision.y,
      params.pointOfCollision.z
    );

    shotDirectionHelper.position.set(
      shooter.position.x,
      shooter.position.y,
      shooter.position.z
    );
    shotDirectionHelper.setDirection(
      new THREE.Vector3(
        params.shotDirection!.x,
        params.shotDirection!.y,
        params.shotDirection!.z
      ).normalize()
    );
  }
  update();

  params.collisionUpdatedListeners.push(update);
}

function createProjectile(root: THREE.Mesh) {
  const geometry = new THREE.SphereGeometry(0.2, 5, 5);
  const material = new THREE.MeshBasicMaterial({
    color: new THREE.Color("#fff"),
  });
  const projectile = new THREE.Mesh(geometry, material);
  projectile.visible = false;
  root.add(projectile);
  return projectile;
}

const shooter = createActor(ShooterData);
const target = createActor(TargetData);

const projectile = createProjectile(shooter);

createCollisionIndicators(ResultData);
scene.add(
  new THREE.GridHelper(
    20,
    20,
    new THREE.Color(0.1, 0.1, 0.1),
    new THREE.Color(0.1, 0.1, 0.1)
  )
);
scene.add(new THREE.AxesHelper(10));

scene.add(new THREE.PointLight());
controls.target = new THREE.Vector3()
  .addVectors(target.position, shooter.position)
  .multiplyScalar(0.5);
controls.update();

let t: undefined | number = undefined;
let isAnimationRunning = () => !!t;

ResultData.runSimulationListeners.push(() => {
  console.log("e");
  if (!isAnimationRunning()) {
    t = 0;
    projectile.visible = true;
  } else {
    t = undefined;
    projectile.visible = false;

    shooter.position.set(
      ShooterData.position.x,
      ShooterData.position.y,
      ShooterData.position.z
    );

    target.position.set(
      TargetData.position.x,
      TargetData.position.y,
      TargetData.position.z
    );
  }
});

let lastFrame: number;

function animate(now: DOMHighResTimeStamp) {
  requestAnimationFrame(animate);
  if (!lastFrame) {
    lastFrame = now;
    return;
  }
  const dT = (now - lastFrame) / 1000;
  lastFrame = now;
  controls.update(dT);
  renderer.render(scene, camera);

  if (typeof t === "undefined") {
    return;
  }

  t += dT;

  shooter.position.set(
    ShooterData.position.x + ShooterData.direction.x * t,
    ShooterData.position.y + ShooterData.direction.y * t,
    ShooterData.position.z + ShooterData.direction.z * t
  );

  if (ResultData.shotDirection) {
    const normalizedDir = new THREE.Vector3(
      ResultData.shotDirection.x,
      ResultData.shotDirection.y,
      ResultData.shotDirection.z
    ).normalize();
    projectile.position.set(
      normalizedDir.x * ResultData.projectileSpeed * t,
      normalizedDir.y * ResultData.projectileSpeed * t,
      normalizedDir.z * ResultData.projectileSpeed * t
    );
  }

  target.position.set(
    TargetData.position.x + TargetData.direction.x * t,
    TargetData.position.y + TargetData.direction.y * t,
    TargetData.position.z + TargetData.direction.z * t
  );
}
animate(undefined!);
