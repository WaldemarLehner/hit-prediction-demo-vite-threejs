import { Pane, TabPageApi } from "tweakpane";
import {
  getPointOfCollision,
  getRequiredShotDirection,
  getTimeOfCollision,
} from "./collisionCalculator";

const pane = new Pane({ title: "Settings" });

const paneTabs = pane.addTab({
  pages: [{ title: "Proj/Res" }, { title: "Shooter" }, { title: "Target" }],
});

const [titleTab, shooterTab, targetTab] = paneTabs.pages;

type Vec3 = { x: number; y: number; z: number };
type Color3 = { r: number; g: number; b: number };

export interface EntityParams {
  position: Vec3;
  direction: Vec3;
  entityColor: Color3;
  focusListeners: ((pos: Vec3) => void)[];
  positionListeners: ((pos: Vec3) => void)[];
  directionListeners: ((pos: Vec3) => void)[];
  colorListeners: ((pos: Color3) => void)[];
}

function setupEntityTab(tab: TabPageApi, initial?: Partial<EntityParams>) {
  const randomPos = () => {
    return {
      x: Math.random() * 10 - 5,
      y: Math.random() * 10 - 5,
      z: Math.random() * 10 - 5,
    };
  };

  const randomDir = () => {
    return {
      x: Math.random() * 2 - 1,
      y: Math.random() * 2 - 1,
      z: Math.random() * 2 - 1,
    };
  };

  const params = initial ?? {};
  params.position ??= randomPos();
  params.direction ??= randomDir();
  params.entityColor ??= { r: 1, g: 0, b: 0 };
  params.focusListeners ??= [];
  params.positionListeners ??= [];
  params.directionListeners ??= [];
  params.colorListeners ??= [];

  tab
    .addBinding(params, "position")
    .on("change", (ev) =>
      (params.positionListeners ?? []).forEach((e) => e(ev.value!))
    );
  tab
    .addBinding(params, "direction")
    .on("change", (ev) =>
      (params.directionListeners ?? []).forEach((e) => e(ev.value!))
    );
  tab
    .addBinding(params, "entityColor", {
      color: { type: "float" },
      r: { min: 0, max: 1 },
      g: { min: 0, max: 1 },
      b: { min: 0, max: 1 },
    })
    .on("change", (ev) =>
      (params.colorListeners ?? []).forEach((e) => e(ev.value!))
    );
  tab.addButton({ title: "Focus on", label: "camera" }).on("click", () => {
    (params.focusListeners ?? []).forEach((e) => e(params.position!));
  });

  return params as EntityParams;
}

export const ShooterData = setupEntityTab(shooterTab, {
  entityColor: { r: 0, g: 1, b: 0 },
});
export const TargetData = setupEntityTab(targetTab, {
  entityColor: { r: 1, g: 0, b: 0 },
});

export const ResultData: {
  timeOfCollision: number;
  pointOfCollision: Vec3 | undefined;
  shotDirection: Vec3 | undefined;
  collisionPositionText: string;
  runSimulationListeners: (() => void)[];
  projectileSpeedChangeListeners: ((speed: number) => void)[];
  collisionUpdatedListeners: ((
    pos: Vec3 | undefined,
    time: number | undefined
  ) => void)[];
  projectileSpeed: number;
} = {
  timeOfCollision: 0,
  collisionPositionText: "not init",
  pointOfCollision: { x: 0, y: 0, z: 0 },
  shotDirection: { x: 0, y: 0, z: 0 },
  runSimulationListeners: [],
  projectileSpeed: 2,
  projectileSpeedChangeListeners: [],
  collisionUpdatedListeners: [],
};
titleTab
  .addBinding(ResultData, "projectileSpeed", { min: 0.1 })
  .on("change", (ev) =>
    ResultData.projectileSpeedChangeListeners.forEach((e) => e(ev.value!))
  );
titleTab.addBinding(ResultData, "timeOfCollision", { readonly: true });
titleTab.addBinding(ResultData, "collisionPositionText", { readonly: true });
titleTab.addButton({ title: "Run/Reset Simulation" }).on("click", () => {
  ResultData.runSimulationListeners.forEach((e) => e());
});

function updateResultData() {
  let result = getTimeOfCollision(
    ShooterData.position,
    ShooterData.direction,
    TargetData.position,
    TargetData.direction,
    ResultData.projectileSpeed
  );

  ResultData.timeOfCollision = result!;
  ResultData.pointOfCollision = getPointOfCollision(
    TargetData.position,
    TargetData.direction,
    result
  )!;

  ResultData.shotDirection = getRequiredShotDirection(
    ShooterData.position,
    ShooterData.direction,
    TargetData.position,
    TargetData.direction,
    result
  );

  if (!ResultData.pointOfCollision) {
    ResultData.collisionPositionText = "cannot collide";
  } else {
    ResultData.collisionPositionText = `( ${[
      ResultData.pointOfCollision.x,
      ResultData.pointOfCollision.y,
      ResultData.pointOfCollision.z,
    ]
      .map((e) => Math.round(e * 100) / 100)
      .join("|")})`;
  }

  ResultData.collisionUpdatedListeners.forEach((e) =>
    e(ResultData.pointOfCollision, ResultData.timeOfCollision)
  );
}

[
  ResultData.projectileSpeedChangeListeners,
  TargetData.directionListeners,
  TargetData.positionListeners,
  ShooterData.directionListeners,
  ShooterData.positionListeners,
].forEach((e) => e.push(() => updateResultData()));

updateResultData();
