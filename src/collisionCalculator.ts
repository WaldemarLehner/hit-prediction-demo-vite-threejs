type Vec3 = { x: number; y: number; z: number };

/**
 * Run an operation on a per-component basis, return the result as a new vector
 */
function vec3Operate(
  a: Readonly<Vec3>,
  b: Readonly<Vec3>,
  operation: (componentA: number, componentB: number) => number
) {
  return {
    x: operation(a.x, b.x),
    y: operation(a.y, b.y),
    z: operation(a.z, b.z),
  } as const;
}

function vec3Sum(a: Readonly<Vec3>) {
  return a.x + a.y + a.z;
}

export function getTimeOfCollision(
  shooterPos: Vec3,
  shooterDir: Vec3,
  targetPos: Vec3,
  targetDir: Vec3,
  projectileSpeed: number
) {
  const localTargetPos = vec3Operate(targetPos, shooterPos, (a, b) => a - b);
  const localTargetDir = vec3Operate(targetDir, shooterDir, (a, b) => a - b);

  const rootLeftSegment =
    -2 * vec3Sum(vec3Operate(localTargetPos, localTargetDir, (a, b) => a * b));
  const rootRightSegment =
    -4 *
    vec3Sum(vec3Operate(localTargetPos, localTargetPos, (a, b) => a * b)) *
    (vec3Sum(vec3Operate(localTargetDir, localTargetDir, (a, b) => a * b)) -
      projectileSpeed * projectileSpeed);
  const rootValue = rootLeftSegment * rootLeftSegment + rootRightSegment;

  if (rootValue < 0) {
    return undefined;
  }

  const root = Math.sqrt(rootValue);
  const bottom =
    -2 *
    (vec3Sum(vec3Operate(localTargetDir, localTargetDir, (a, b) => a * b)) -
      projectileSpeed * projectileSpeed);

  const result1 = (-rootLeftSegment - root) / bottom;
  const result2 = (-rootLeftSegment + root) / bottom;

  return [result1, result2].sort().find((e) => e > 0);
}

export function getPointOfCollision(
  targetPos: Vec3,
  targetDir: Vec3,
  t: number | undefined
) {
  if (!t) {
    return undefined;
  }
  const directionAfterTime = vec3Operate(
    targetDir,
    { x: t, y: t, z: t } as const,
    (a, b) => a * b
  );
  return vec3Operate(targetPos, directionAfterTime, (a, b) => a + b);
}

export function getRequiredShotDirection(
  shooterPos: Vec3,
  shooterDir: Vec3,
  targetPos: Vec3,
  targetDir: Vec3,
  t: number | undefined
) {
  if (!t) {
    return undefined;
  }
  const localTargetPos = vec3Operate(targetPos, shooterPos, (a, b) => a - b);
  const localTargetDir = vec3Operate(targetDir, shooterDir, (a, b) => a - b);

  const pointOfCollision = getPointOfCollision(
    localTargetPos,
    localTargetDir,
    t
  )!;

  return pointOfCollision;
}
