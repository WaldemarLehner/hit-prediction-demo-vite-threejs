import { defineConfig } from "vite";

function getBase(useGitlab: boolean) {
  if (useGitlab) {
    return process.env.CI_PAGES_URL + "/";
  }
  return "/";
}

// https://vitejs.dev/config/
export default defineConfig({
  base: getBase(process.env.GITLAB_BUILD == "1"),
});
